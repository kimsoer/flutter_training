import 'package:flutter_lesson_1/core/enum/local_service.dart';
import 'package:flutter_lesson_1/core/services/api_provider_service.dart';
import 'package:flutter_lesson_1/core/services/local_service.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:stacked/stacked.dart';

import '../model/response/article_response.dart';

class HomeViewModel extends BaseViewModel {
  List<ArticleResponse> articleList = [];
  RefreshController refreshController =
      RefreshController(initialRefresh: false);
  getInstance() async {
    clearErrors();
    setBusy(false);
    ApiProviderService().getInstance();
    await LocalService().getInstance();
    getProduct();

    setInitialised(true);
    refreshController.refreshCompleted();
    notifyListeners();
  }

  getProduct() async {
    try {
      setBusy(true);
      notifyListeners();

      var responseData =
          await ApiProviderService().getRestClient().getArticles();

      LocalService()
          .saveValue(LocalDataFieldName.CUSTOMER_JOSN, responseData.data);
      for (var data in responseData.data) {
        ArticleResponse dataProduct = ArticleResponse.fromJson(data);
        articleList.add(dataProduct);
      }

      var dataLocal =
          await LocalService().getSavedValue(LocalDataFieldName.CUSTOMER_JOSN);

      print("data response=> ${dataLocal.toString()}");
    } catch (error) {
      print("data error=> ${error.toString()}");
    } finally {
      setBusy(false);
      notifyListeners();
    }
  }
}
