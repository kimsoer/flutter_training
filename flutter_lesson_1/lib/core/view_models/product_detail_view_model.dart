import 'package:flutter_lesson_1/core/model/response/article_detail_response.dart';
import 'package:flutter_lesson_1/core/services/api_provider_service.dart';
import 'package:stacked/stacked.dart';

class ProductDetailViewModel extends BaseViewModel {
  ArticleDetailResponse articleDetailResponse = ArticleDetailResponse();

  getInstance(int id) {
    clearErrors();
    setBusy(false);

    getArticle(id);

    setInitialised(true);
    notifyListeners();
  }

  getArticle(int id) async {
    try {
      setBusy(true);
      notifyListeners();
      articleDetailResponse =
          await ApiProviderService().getRestClient().getArticle(id);
      print("data view=>${articleDetailResponse.body}");
    } catch (error) {
      print("error => ${error}");
    } finally {
      setBusy(false);
      notifyListeners();
    }
  }
}
