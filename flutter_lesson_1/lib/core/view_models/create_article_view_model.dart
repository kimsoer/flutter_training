import 'package:art_sweetalert/art_sweetalert.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_lesson_1/core/model/request/article_request.dart';
import 'package:flutter_lesson_1/core/services/api_provider_service.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:stacked/stacked.dart';

class CreateArticleViewModel extends BaseViewModel {
  RefreshController refreshController =
      RefreshController(initialRefresh: false);
  TextEditingController tecId = TextEditingController();
  TextEditingController tecUserId = TextEditingController();
  TextEditingController tecTitle = TextEditingController();
  TextEditingController tecBody = TextEditingController();
  getInstance() async {
    clearErrors();
    setBusy(false);

    setInitialised(true);
    refreshController.refreshCompleted();
    notifyListeners();
  }

  save(BuildContext context) async {
    try {
      setBusy(true);
      notifyListeners();
      ArticleRequest articleRequest = ArticleRequest();
      articleRequest.id = int.parse(tecId.text);
      articleRequest.userId = int.parse(tecUserId.text);
      articleRequest.title = tecTitle.text;
      articleRequest.body = tecBody.text;
      var response =
          await ApiProviderService().getRestClient().saveToDB(articleRequest);
      if (response.response.statusCode == 201) {
        ArtSweetAlert.show(
            context: context,
            artDialogArgs: ArtDialogArgs(
                type: ArtSweetAlertType.success,
                title: "A success message!",
                text: "Show a success message with an icon"));
      }
      print("data response => ${response.response.toString()}");
    } catch (error) {
      print("error=> ${error.toString()}");
    } finally {
      setBusy(false);
      notifyListeners();
    }
  }

  clearForm() {
    tecId.clear();
    tecUserId.clear();
    tecBody.clear();
    tecTitle.clear();
  }
}
