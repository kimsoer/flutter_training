import 'package:flutter/material.dart';

class GlobalService {
  GlobalService._apiConstructor();

  static final GlobalService _instance = GlobalService._apiConstructor();

  factory GlobalService() {
    return _instance;
  }

  final GlobalKey<NavigatorState> _navigator = GlobalKey<NavigatorState>();

  GlobalKey<NavigatorState> get navigator => _navigator;
  BuildContext get context => _navigator.currentContext!;

  pushNavigation(Widget target, {Function? callBack}) {
    _navigator.currentState!
        .push(
      MaterialPageRoute(builder: (context) => target),
    )
        .then(
      (value) {
        if (value != null) {
          callBack?.call(value);
        } else {
          callBack?.call();
        }
      },
    );
  }

  pushReplacementNavigation(Widget target) {
    _navigator.currentState!.pushReplacement(
      MaterialPageRoute(builder: (context) => target),
    );
  }

  popNavigation({dynamic result}) {
    _navigator.currentState!.pop(result);
  }
}
