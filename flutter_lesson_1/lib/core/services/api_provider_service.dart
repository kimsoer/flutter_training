import 'package:dio/dio.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter_lesson_1/core/repository/rest_client_repository.dart';
import 'package:flutter_lesson_1/core/utils/config.dart';
import 'package:pretty_dio_logger/pretty_dio_logger.dart';

class ApiProviderService {
  ApiProviderService._apiConstructor();

  static final ApiProviderService _instance =
      ApiProviderService._apiConstructor();

  factory ApiProviderService() {
    return _instance;
  }

  final Dio _dio = Dio(
    BaseOptions(
      connectTimeout: 10000,
      sendTimeout: 10000,
      receiveTimeout: 10000,
      baseUrl: BASE_URL,
      headers: {
        "Content-Type": "application/json",
        "Accept": "application/json",
      },
    ),
  );

  Dio get getDioInstance => _dio;

  RestClientRepository getRestClient() {
    return RestClientRepository(ApiProviderService().getDioInstance,
        baseUrl: BASE_URL);
  }

  getInstance() {
    if (!kReleaseMode) {
      _dio.interceptors.add(
        PrettyDioLogger(
          requestHeader: true,
          requestBody: true,
          responseBody: true,
          responseHeader: false,
          error: true,
          compact: true,
        ),
      );
    }
  }

  setUserAccessToken(String accessToken) {
    _dio.options.headers = {
      "Content-Type": "application/json",
      "Accept": "application/json",
      "Authorization": "Bearer $accessToken",
    };
  }

  clearUserAccessToken() {
    _dio.options.headers = {
      "Content-Type": "application/json",
      "Accept": "application/json",
    };
  }
}
