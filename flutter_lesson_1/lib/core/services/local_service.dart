import 'dart:convert';

import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:hive/hive.dart';
import 'package:path_provider/path_provider.dart';

import '../enum/local_service.dart';

class LocalService {
  static LocalService? _instance;
  LocalService._();

  factory LocalService() {
    if (_instance == null) {
      _instance = LocalService._();
    }
    return _instance!;
  }
  late Box _localObjectBox;

  getInstance() async {
    final directory = await getApplicationDocumentsDirectory();
    final storage = FlutterSecureStorage();
    String? encryptKey = await storage.read(key: "key");

    if (encryptKey == null) {
      var key1 = Hive.generateSecureKey();
      await storage.write(key: "key", value: base64.encode(key1));
      encryptKey = await storage.read(key: "key");
    }
    List<int> key = base64.decode(encryptKey!);

    Hive..init(directory.path);

    _localObjectBox =
        await Hive.openBox('APP_DATA', encryptionCipher: HiveAesCipher(key));
  }

  saveValue(LocalDataFieldName localDataFieldName, dynamic value) async {
    if (_localObjectBox == null) {
      await getInstance();
    }
    _localObjectBox.put(localDataFieldName.toString(), value);
  }

  dynamic getSavedValue(LocalDataFieldName localDataFieldName) async {
    if (_localObjectBox == null) {
      await getInstance();
    }
    return _localObjectBox.get(localDataFieldName.toString());
  }

  deleteSavedValue(LocalDataFieldName localDataFieldName) async {
    if (_localObjectBox == null) {
      await getInstance();
    }

    _localObjectBox.delete(localDataFieldName.toString());
  }

  deleteAllSavedValue() async {
    if (_localObjectBox == null) {
      await getInstance();
    }
    _localObjectBox.clear();
  }
}
