import 'package:dio/dio.dart';
import 'package:flutter_lesson_1/core/model/request/article_request.dart';
import 'package:flutter_lesson_1/core/model/response/article_detail_response.dart';
import 'package:flutter_lesson_1/core/utils/config.dart';
import 'package:retrofit/retrofit.dart';

part 'rest_client_repository.g.dart';

@RestApi(baseUrl: BASE_URL)
abstract class RestClientRepository {
  factory RestClientRepository(Dio dio, {String baseUrl}) =
      _RestClientRepository;

  @GET("posts")
  Future<HttpResponse> getArticles();

  @GET("posts/{id}")
  Future<ArticleDetailResponse> getArticle(@Path() int id);

  @POST("posts")
  Future<HttpResponse> saveToDB(@Body() ArticleRequest articleRequest);
}
