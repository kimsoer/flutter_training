import 'package:flutter/material.dart';
import 'package:stacked/stacked.dart';

class ViewModelStateOverlay<T extends BaseViewModel>
    extends ViewModelWidget<T> {
  ViewModelStateOverlay({
    required this.child,
    this.appBarTitle,
    this.onDismissed,
  });

  final Widget child;
  final Function? onDismissed;
  final String? appBarTitle;

  @override
  Widget build(BuildContext context, T viewModel) {
    return Material(
      color: Theme.of(context).scaffoldBackgroundColor,
      child: Stack(
        fit: StackFit.expand,
        children: [
          viewModel.initialised
              ? this.child
              : Scaffold(
                  appBar: AppBar(
                    title: Text("${appBarTitle ?? ""}"),
                  ),
                  body: Center(
                    child: CircularProgressIndicator(),
                  ),
                ),
          if (viewModel.isBusy)
            Container(
              color: Colors.black54,
              child: Center(
                child: Container(
                  padding: const EdgeInsets.all(20),
                  margin: const EdgeInsets.all(32),
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(16),
                  ),
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Text(
                        "Please wait",
                        style: Theme.of(context).textTheme.bodyText1,
                      ),
                      Container(
                        margin: const EdgeInsets.symmetric(
                          vertical: 16,
                        ),
                        child: CircularProgressIndicator(),
                      ),
                      Text(
                        "We are processing your request",
                        style: Theme.of(context).textTheme.caption,
                      ),
                    ],
                  ),
                ),
              ),
            ),
          if (viewModel.hasError)
            GestureDetector(
              onTap: () {
                onDismissed?.call();
                viewModel.clearErrors();
                viewModel.notifyListeners();
              },
              child: Container(
                color: Colors.black54,
                child: Center(
                  child: Container(
                    padding: const EdgeInsets.all(20),
                    margin: const EdgeInsets.all(32),
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(16),
                    ),
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      children: [
                        Text(
                          "Connected to Server Failed",
                          style: Theme.of(context).textTheme.subtitle1!.apply(
                                color: Colors.red,
                              ),
                        ),
                        Container(
                          height: 1,
                          margin: const EdgeInsets.symmetric(
                            vertical: 8,
                          ),
                          color: Colors.grey[300],
                        ),
                        Text(
                          "${viewModel.modelError ?? "Something went wrong. Please try again."}",
                          style: Theme.of(context).textTheme.bodyText1,
                        ),
                        SizedBox(
                          height: 16,
                        ),
                        TextButton(
                          onPressed: () async {
                            onDismissed?.call();
                            viewModel.clearErrors();
                            viewModel.notifyListeners();
                          },
                          style: TextButton.styleFrom(),
                          child: Text(
                            onDismissed == null ? "Okay" : "Try again",
                            style: Theme.of(context).textTheme.bodyText1,
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ),
        ],
      ),
    );
  }
}
