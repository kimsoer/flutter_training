import 'package:flutter/material.dart';
import 'package:flutter_lesson_1/core/view_models/home_view_model.dart';
import 'package:flutter_lesson_1/ui/view/product_detail/product_detail_view.dart';
import 'package:flutter_lesson_1/ui/widget/view_model_state_overlay.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:stacked/stacked.dart';

class HomeView extends ViewModelBuilderWidget<HomeViewModel> {
  @override
  void onViewModelReady(HomeViewModel viewModel) {
    viewModel.getInstance();
  }

  @override
  HomeViewModel viewModelBuilder(BuildContext context) {
    return HomeViewModel();
  }

  @override
  Widget builder(BuildContext context, HomeViewModel viewModel, Widget? child) {
    var data = viewModel.articleList;
    return ViewModelStateOverlay<HomeViewModel>(
        child: Scaffold(
      appBar: AppBar(
        title: Text("Amret News"),
      ),
      body: SmartRefresher(
        controller: viewModel.refreshController,
        onRefresh: viewModel.getInstance,
        child: ListView.builder(
            itemCount: data.length,
            itemBuilder: (context, index) {
              return GestureDetector(
                onTap: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => ProductDetailView(
                        productId: data[index].id,
                        image:
                            "https://i.picsum.photos/id/1035/200/200.jpg?hmac=IDuYUZQ_7a6h4pQU2k7p2nxT-MjMt4uy-p3ze94KtA4",
                      ),
                    ),
                  );
                },
                child: Card(
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      ClipRRect(
                        borderRadius: BorderRadius.circular(10),
                        child: Image.network(
                          "https://i.picsum.photos/id/1035/200/200.jpg?hmac=IDuYUZQ_7a6h4pQU2k7p2nxT-MjMt4uy-p3ze94KtA4",
                          width: 150,
                          height: 150,
                        ),
                      ),
                      SizedBox(
                        width: 10,
                      ),
                      Expanded(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              "${data[index].title}".toUpperCase(),
                              maxLines: 1,
                              style: TextStyle(
                                  fontWeight: FontWeight.bold, fontSize: 18),
                              overflow: TextOverflow.ellipsis,
                            ),
                            Text(
                              "${data[index].body}",
                              maxLines: 4,
                              textAlign: TextAlign.justify,
                              overflow: TextOverflow.ellipsis,
                            ),
                            SizedBox(
                              height: 5,
                            ),
                            Text(
                              "By Author",
                              style: TextStyle(color: Colors.grey),
                            ),
                            SizedBox(
                              height: 5,
                            ),
                            Row(
                              children: [
                                Icon(Icons.favorite_border),
                                Text("${data[index].id}"),
                                SizedBox(
                                  width: 10,
                                ),
                                Icon(Icons.comment),
                                Text("${data[index].userId}"),
                                SizedBox(
                                  width: 10,
                                ),
                                Text(
                                  "${data[index].userId} minutes ago",
                                  style: TextStyle(color: Colors.grey),
                                )
                              ],
                            )
                          ],
                        ),
                      )
                    ],
                  ),
                ),
              );
            }),
      ),
    ));
  }
}
