import 'package:flutter/material.dart';
import 'package:flutter_lesson_1/core/view_models/create_article_view_model.dart';
import 'package:stacked/stacked.dart';

class CreateArticleView extends ViewModelBuilderWidget<CreateArticleViewModel> {
  @override
  void onViewModelReady(CreateArticleViewModel viewModel) {
    viewModel.getInstance();
  }

  @override
  CreateArticleViewModel viewModelBuilder(BuildContext context) {
    return CreateArticleViewModel();
  }

  @override
  Widget builder(
      BuildContext context, CreateArticleViewModel viewModel, Widget? child) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Create Article"),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Container(
              padding: const EdgeInsets.all(10),
              child: TextField(
                controller: viewModel.tecId,
                keyboardType: TextInputType.text,
                decoration: InputDecoration(
                  label: Text("Input your id"),
                  labelStyle: TextStyle(fontSize: 16, color: Colors.black54),
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(8),
                    borderSide: BorderSide(
                      width: 0,
                      style: BorderStyle.none,
                    ),
                  ),
                  filled: true,
                  contentPadding: EdgeInsets.all(16),
                ),
              ),
            ),
            Container(
              padding: const EdgeInsets.all(10),
              child: TextField(
                controller: viewModel.tecUserId,
                keyboardType: TextInputType.text,
                decoration: InputDecoration(
                  label: Text("Input your username"),
                  labelStyle: TextStyle(fontSize: 16, color: Colors.black54),
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(8),
                    borderSide: BorderSide(
                      width: 0,
                      style: BorderStyle.none,
                    ),
                  ),
                  filled: true,
                  contentPadding: EdgeInsets.all(16),
                ),
              ),
            ),
            Container(
              padding: const EdgeInsets.all(10),
              child: TextField(
                controller: viewModel.tecTitle,
                keyboardType: TextInputType.text,
                decoration: InputDecoration(
                  label: Text("Input your title"),
                  labelStyle: TextStyle(fontSize: 16, color: Colors.black54),
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(8),
                    borderSide: BorderSide(
                      width: 0,
                      style: BorderStyle.none,
                    ),
                  ),
                  filled: true,
                  contentPadding: EdgeInsets.all(16),
                ),
              ),
            ),
            Container(
              padding: const EdgeInsets.all(10),
              child: TextField(
                controller: viewModel.tecBody,
                maxLines: 10,
                textAlign: TextAlign.start,
                keyboardType: TextInputType.text,
                decoration: InputDecoration(
                  label: Text("Input your body content"),
                  labelStyle: TextStyle(fontSize: 16, color: Colors.black54),
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(8),
                    borderSide: BorderSide(
                      width: 0,
                      style: BorderStyle.none,
                    ),
                  ),
                  filled: true,
                  contentPadding: EdgeInsets.all(16),
                ),
              ),
            ),
            Container(
              width: MediaQuery.of(context).size.width,
              child: Row(
                children: [
                  Expanded(
                      child: ElevatedButton(
                          onPressed: () {
                            viewModel.save(context);
                          },
                          child: Text("Save"))),
                  SizedBox(
                    width: 10,
                  ),
                  Expanded(
                      child: ElevatedButton(
                          onPressed: () {
                            viewModel.clearForm();
                          },
                          child: Text("Clear"))),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
