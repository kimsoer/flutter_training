import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_lesson_1/core/view_models/product_detail_view_model.dart';
import 'package:flutter_lesson_1/ui/view/article/create_article_view.dart';
import 'package:flutter_lesson_1/ui/widget/view_model_state_overlay.dart';
import 'package:stacked/stacked.dart';

class ProductDetailView extends ViewModelBuilderWidget<ProductDetailViewModel> {
  final int? productId;
  final String? image;
  ProductDetailView({this.productId, this.image});
  @override
  void onViewModelReady(ProductDetailViewModel viewModel) {
    viewModel.getInstance(productId ?? 0);
  }

  @override
  ProductDetailViewModel viewModelBuilder(BuildContext context) {
    return ProductDetailViewModel();
  }

  @override
  Widget builder(
      BuildContext context, ProductDetailViewModel viewModel, Widget? child) {
    var data = viewModel.articleDetailResponse;
    return ViewModelStateOverlay<ProductDetailViewModel>(
      child: Scaffold(
        appBar: AppBar(
          title: Text("Product Detail"),
        ),
        body: Card(
          child: Container(
            width: MediaQuery.of(context).size.width,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                CachedNetworkImage(
                  imageUrl: image ?? '',
                  width: 300,
                  height: 300,
                  fit: BoxFit.cover,
                  progressIndicatorBuilder: (context, url, downloadProgress) =>
                      CircularProgressIndicator(
                          value: downloadProgress.progress),
                  errorWidget: (context, url, error) => const Icon(
                    Icons.error,
                    size: 50,
                  ),
                ),
                Container(
                  padding: const EdgeInsets.only(left: 10),
                  width: MediaQuery.of(context).size.width / 2,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        data.title ?? '',
                        style: const TextStyle(
                            fontSize: 20, fontWeight: FontWeight.bold),
                      ),
                      const Text("Description",
                          style: TextStyle(
                              fontSize: 18, fontWeight: FontWeight.bold)),
                      Text(data.body ?? ''),
                      Text("${data.title ?? 0}"),
                    ],
                  ),
                )
              ],
            ),
          ),
        ),
        floatingActionButton: FloatingActionButton(
          onPressed: () {
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => CreateArticleView()),
            );
          },
          child: Icon(Icons.add),
        ),
      ),
    );
  }
}
